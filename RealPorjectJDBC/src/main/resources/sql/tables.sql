create table tbl_catagory(
id int primary key auto_increment,
catagoryname varchar(50)
);

create table tbl_product(
id int primary key auto_increment,
name varchar(50),
price decimal,
qty int,
catagory_id int references tbl_catagory(id),
import_date varchar(100),
image varchar(100)
)