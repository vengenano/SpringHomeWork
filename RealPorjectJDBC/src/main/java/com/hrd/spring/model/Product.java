package com.hrd.spring.model;

public class Product {
	int id;
	String name;
	double price;
	int qty;
	Catagory catagory;
	String importDate;
	String image;
	public Product(){
	}
	public Product(int id, String name, double price, int qty, Catagory catagory, String importDate,
			String image) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.qty = qty;
		this.catagory = catagory;
		this.importDate = importDate;
		this.image = image;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public Catagory getCatagory() {
		return catagory;
	}
	public void setCatagory(Catagory catagory) {
		this.catagory = catagory;
	}
	public String getImportDate() {
		return importDate;
	}
	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + ", qty=" + qty + ", catagory=" + catagory
				+ ", importDate=" + importDate + ", image=" + image + "]";
	};
	
}