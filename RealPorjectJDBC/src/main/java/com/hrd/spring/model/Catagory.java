package com.hrd.spring.model;

public class Catagory {
	int id;
	String catagory;
	public Catagory() {
		
	}

	public Catagory(int id, String catagory) {
		super();
		this.id = id;
		this.catagory = catagory;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCatagory() {
		return catagory;
	}
	public void setCatagory(String catagory) {
		this.catagory = catagory;
	}
	@Override
	public String toString() {
		return "Catagory [id=" + id + ", catagory=" + catagory + "]";
	}
	
}
