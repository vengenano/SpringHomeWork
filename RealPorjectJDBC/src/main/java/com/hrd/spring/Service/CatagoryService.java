package com.hrd.spring.Service;

import java.util.List;

import com.hrd.spring.model.Catagory;

public interface CatagoryService {
	List<Catagory> selectAll();
	Catagory selectOne(int id);
}
