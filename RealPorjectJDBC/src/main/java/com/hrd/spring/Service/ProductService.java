package com.hrd.spring.Service;

import java.util.List;
import com.hrd.spring.model.Product;
import com.hrd.spring.model.ProductFillter;

import ch.qos.logback.core.filter.Filter;

public interface ProductService {
	public void add(Product product);
	public void delete(int id);
	public Product findOne(int id);
	public List<Product> findAll();
	public void update(Product product);
	//public List<Product> fillterAllProduct(ProductFillter filter);
}
