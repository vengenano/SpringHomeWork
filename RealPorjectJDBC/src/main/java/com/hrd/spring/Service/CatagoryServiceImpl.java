package com.hrd.spring.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hrd.spring.Repository.CatagoryRepository;
import com.hrd.spring.model.Catagory;
@Service
public class CatagoryServiceImpl implements CatagoryService {
	
	@Autowired
	private CatagoryRepository catagoryRepository;
	
	@Override
	public List<Catagory> selectAll() {
		return catagoryRepository.selectAll();
	}
	
	@Override
	public Catagory selectOne(int id) {
		return catagoryRepository.selectOne(id);
	}

}
