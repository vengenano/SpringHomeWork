package com.hrd.spring.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hrd.spring.Repository.ProductRepository;
import com.hrd.spring.model.Product;
@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	public ProductRepository productRepository;
//	@Autowired
//	ProductFillter productFillter;
	@Override
	public void add(Product product) {
		productRepository.add(product);
	}
	@Override
	public void delete(int id) {
		productRepository.delete(id);
	}
	@Override
	public Product findOne(int id) {
		return productRepository.findOne(id);
	}
	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}
	@Override
	public void update(Product product) {	
		productRepository.update(product);
	}
	
//	@Override
//	public List<Product> fillterAllProduct(ProductFillter filter) {
//		
//		return productRepository.fillterAllProduct(filter);
//	}
	

}
