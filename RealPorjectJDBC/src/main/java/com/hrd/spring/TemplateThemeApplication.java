package com.hrd.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemplateThemeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemplateThemeApplication.class, args);
	}
}
