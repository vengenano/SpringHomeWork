package com.hrd.spring.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hrd.spring.Service.CatagoryService;
import com.hrd.spring.Service.ProductService;
import com.hrd.spring.model.Product;
import com.hrd.spring.model.ProductFillter;
@Controller
public class MainController {
	@Autowired
	public CatagoryService catagoryService;
	
	@Value("${file.server.path}")
	String serverPath;
	
	@Autowired
	public ProductService productService;
	
	@GetMapping("/back")
	public String back(){
		return "redirect:/dashboard";
	}
	@RequestMapping("/admin")
	public String admin() {
		return "admin_layout";
	}
	@RequestMapping("/dashboard")
	public String homeProduct(Model model) {
		
//		List<Product> productList=productService.findAll();
		//model.addAttribute("products", productService.findAll());
		model.addAttribute("products", productService.findAll());
		//System.out.println(productService.findAll());
		//System.out.println(productList);
		return "dashboard";
	}
	@RequestMapping("/insert")
	public String insert(Model model) {
		model.addAttribute("Form",true);
		model.addAttribute("catagories",catagoryService.selectAll());
		model.addAttribute("productLists",new Product());
		return "insert";
	}
	@PostMapping("/insert")
	public String save(@RequestParam("file") MultipartFile file,@ModelAttribute Product productList,BindingResult result) {
//		if(result.hasErrors()) {
//			return "redirect:/dashboard";
//		}
		String fileName="";
		if(!file.isEmpty()) {
			fileName=UUID.randomUUID()+file.getOriginalFilename();
			try {
				Files.copy(file.getInputStream(),Paths.get(serverPath,fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		};
		//productList.setImportDate(new Date().toString());
		productList.setImage("/image/"+fileName);
		
		productService.add(productList);
		//System.err.println(product);
		return "redirect:/dashboard";
	}
	
	@GetMapping("/delete/{id}")
	public String deleted(@PathVariable("id") int id) {
		//System.out.println("ID :"+id);
		productService.delete(id);
		return "redirect:/dashboard";
		//return "insert";
	}	
	@GetMapping("/view/{id}")
	public String viewData(@PathVariable("id") int id,Model model){
		Product products=productService.findOne(id);
		model.addAttribute("products",products);
		return "view_detail";
	}
	@RequestMapping("/update/{id}")
	public String update(@PathVariable("id") int id,Model model) {
		Product productList=productService.findOne(id);
		model.addAttribute("Form",false);
		model.addAttribute("catagories",catagoryService.selectAll());
		model.addAttribute("productLists",productList);
		return "insert";
	}
	@PostMapping("/update")
	public String update(@RequestParam("file") MultipartFile file, @ModelAttribute Product productList) {
	
		String fileName="";
		if(!file.isEmpty()) {
			fileName=UUID.randomUUID()+file.getOriginalFilename();
			try {
				Files.copy(file.getInputStream(),Paths.get(serverPath,fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		};
		productList.setImportDate(new Date().toString());
		productList.setImage("/image/"+fileName);
		productService.update(productList);
		return "redirect:/dashboard";
	}
	
}
