package com.hrd.spring.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DatabaseConfiguration {
	@Bean
	@Profile("kps")
	public DataSource dataSource() {
		DriverManagerDataSource db=new DriverManagerDataSource();
		db.setDriverClassName("org.postgresql.Driver");
		db.setUrl("jdbc:postgresql://localhost:5432/mybatisDB");
		db.setUsername("postgres");
		db.setPassword("venge");
		return db;
	}
	@Bean
	@Profile("sr")
	public DataSource sr() {
		DriverManagerDataSource db=new DriverManagerDataSource();
		db.setDriverClassName("org.postgresql.Driver");
		db.setUrl("jdbc:postgresql://localhost:5432/mybatisDB");
		db.setUsername("postgres");
		db.setPassword("venge");
		return db;
	}
	
	@Bean
	@Profile("virtual")
	public DataSource virtual() {
		EmbeddedDatabaseBuilder builder=new EmbeddedDatabaseBuilder();
		builder.setType(EmbeddedDatabaseType.H2);
		builder.addScripts("sql/tables.sql","sql/data.sql");
		return builder.build();
	}

}
