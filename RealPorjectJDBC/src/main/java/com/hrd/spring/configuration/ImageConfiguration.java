package com.hrd.spring.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@PropertySource("classpath:product.properties")
public class ImageConfiguration extends WebMvcConfigurerAdapter{
	@Value("${file.server.path}")
	String serverPath;
	@Value("${file.client.path}")
	String clientPath;
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(clientPath+"/**").addResourceLocations("file:"+serverPath);
	}
	
}
