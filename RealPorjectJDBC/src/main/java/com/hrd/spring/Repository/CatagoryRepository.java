package com.hrd.spring.Repository;
import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import com.hrd.spring.model.Catagory;

@Repository
public interface CatagoryRepository {
	
	@Select("SELECT id,catagoryname from tbl_catagory")
	@Results({
 		@Result(property="id",column="id"),
		@Result(property="catagory",column="catagoryname")
	})
	public List<Catagory> selectAll();
	
	@Select("SELECT id,catagoryname from tbl_catagory where id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="catagory",column="catagoryname")
	})
	public Catagory selectOne(int id);

}
