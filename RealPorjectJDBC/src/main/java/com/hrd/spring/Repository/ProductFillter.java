package com.hrd.spring.Repository;
import org.apache.ibatis.jdbc.SQL;

public class ProductFillter {
	
	public String fillterAllProduct(com.hrd.spring.model.ProductFillter filter) {
		return new SQL() {{
			SELECT("p.id,p.name,p.price,p.qty,cat.catagoryname,p.import_date,p.image");
			FROM ("tbl_product as p");
			INNER_JOIN("tbl_catagory as c on p.catagory_id=c.id");
			if(filter.getTitle()!=null) {
				WHERE("p.name ilike '%' || #{title} || '%' ");
			}
			if(filter.getCate_id()!=null) {
				WHERE("p.catagory_id = #{cate_id}");
			}
			ORDER_BY("p.id asc");		
		}}.toString();	
	}
}
