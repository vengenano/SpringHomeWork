package com.hrd.spring.Repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.hrd.spring.model.Product;
import com.hrd.spring.model.ProductFillter;
@Repository
public interface ProductRepository {
	
	@Insert("INSERT INTO tbl_product(name,price,qty,catagory_id,image) values(#{name},#{price},#{qty},#{catagory.id},#{image})")
	public void add(Product product);
	
	@Select("select p.id,p.name,p.price,p.qty,cat.catagoryname,p.import_date,p.image "
			+ "from tbl_product as p inner join tbl_catagory as cat"
			+ " on p.catagory_id =cat.id order by p.id asc")
	@Results({
		@Result(property="catagory.catagory",column="catagoryname"),
		@Result(property="importDate",column="import_date"),
		@Result(property="catagory.id",column="catagory_id")
	})
	public List<Product> findAll();
	@Delete("delete from tbl_product where id=#{id}")
	@Results({
		@Result(property="id",column="id")
	})
	public void delete(int id);
	
	@Select("select p.id,p.name,p.price,p.qty,cat.catagoryname,p.import_date,p.image"
			+ " from tbl_product as p inner join tbl_catagory as cat"
			+ " on p.catagory_id =cat.id WHERE p.id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="name",column="name"),
		@Result(property="price",column="price"),
		@Result(property="qty",column="qty"),
		@Result(property="importDate",column="import_date"),
		@Result(property="image",column="image"),
		@Result(property="catagory.id",column="catagory_id"),
		@Result(property="catagory.catagory",column="catagoryname")
	})
	public Product findOne(int id);

	@Update("update tbl_product set name=#{name},price=#{price},qty=#{qty},catagory_id=#{catagory.id},image=#{image} where id=#{id}")
	public void update(Product product);
	
	//public List<Product> fillterAllProduct(ProductFillter filter);
}








